<div align="center"><h1>GTA LIKE PLATE</h1></div>

This is how gta online plate can look like. This can be used as warning, welcome, ban reason message that will show to player when join the game.

![example](example.png)

This is just a desing, and does not have implemented in game mechanics to show it or ban players.

## Preview
This preview show you how this can look like inside your game server when will be added and implemented.

![example2](example2.png)